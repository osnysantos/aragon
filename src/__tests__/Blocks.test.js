import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import React from 'react'
import Blocks from '../components/Blocks'
import { shallow } from 'enzyme'

Enzyme.configure({ adapter: new Adapter() })

describe('Blocks tests', () => {
  beforeEach(() => {})
  it('should render correctly.', () => {
    const wrapper = shallow(<Blocks />)
    expect(wrapper).toMatchSnapshot()
  })
  it('should show feedback when blocks are not loaded yet', () => {
    const wrapper = shallow(<Blocks />)
    expect(wrapper.find('.loading')).toHaveLength(1)
  })
})
