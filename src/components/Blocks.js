import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import { navigate } from '@reach/router'
import { filterEtherTransactions } from '../utils.js'
import storeContext from './../store'
import * as actions from './../actions'
import { Tag, LoadingRing, textStyle, Switch } from '@aragon/ui'
import { HeaderStyle, LoadingStyle } from './styles'
import BlockList from './BlockList'

const Blocks = () => {
  const state = useContext(storeContext)
  const {
    blocks,
    setCurrentBlock,
    setTransactions,
    autoFetch,
    setAutoFetch
  } = state

  const goToTransaction = block => {
    let current = { ...block }
    setCurrentBlock(current)
    setTransactions([])
    navigate(`block/${block.number}`)
    actions.getBlock(block.number).then(block => {
      actions.getTransactions(block.transactions).then(value => {
        current.transactionsObj = value
        setTransactions(filterEtherTransactions(value))
      })
    })
  }

  if (!blocks.length) {
    return (
      <LoadingStyle className="loading">
        <LoadingRing />
        <div>Fetching latest blocks</div>
      </LoadingStyle>
    )
  }

  return (
    <>
      <HeaderStyle>
        <h1
          css={`
            ${textStyle('title2')};
          `}
        >
          Latest Blocks
          <Tag background="transparent">
            &nbsp;
            <Switch checked={autoFetch} onChange={setAutoFetch} />{' '}
            {autoFetch ? `Auto Fetching...` : `Auto Fetch`}
          </Tag>
        </h1>
      </HeaderStyle>
      <BlockList blocks={blocks} goToTransaction={goToTransaction} />
    </>
  )
}

Blocks.propTypes = {
  blocks: PropTypes.array
}

Blocks.defaultProps = {
  blocks: []
}

export default Blocks
