import React, { useEffect, useContext } from 'react'
import { getDate, filterEtherTransactions } from '../utils.js'
import PropTypes from 'prop-types'
import {
  Box,
  LoadingRing,
  IdentityBadge,
  Button,
  IconArrowLeft,
  textStyle
} from '@aragon/ui'
import storeContext from './../store'
import * as actions from './../actions.js'
import TransactionList from './TransactionList'
import { navigate } from '@reach/router'
import { HeaderStyle, LoadingStyle } from './styles'

const Transactions = ({ blockNumber }) => {
  const state = useContext(storeContext)
  const { setCurrentBlock, setTransactions } = state
  useEffect(() => {
    if (currentBlock === null || !state.blocks.length) {
      actions.getBlock(blockNumber).then(block => {
        setCurrentBlock(block)
        actions.getTransactions(block.transactions).then(transactions => {
          setTransactions(filterEtherTransactions(transactions))
        })
      })
    }
  }, [])

  let { currentBlock, transactions } = state
  console.warn('trnasactions', currentBlock)
  if (!currentBlock) {
    return (
      <LoadingStyle className="loading">
        <LoadingRing />
        <div>Fetching block</div>
      </LoadingStyle>
    )
  }
  return (
    <div>
      <HeaderStyle>
        <h1
          css={`
            ${textStyle('title2')};
          `}
        >
          Block #{blockNumber}
        </h1>
        <Button
          icon={<IconArrowLeft />}
          label="Back"
          size="small"
          onClick={() => navigate('/')}
        />
      </HeaderStyle>
      <Box>
        <table width="100%">
          <tbody>
            <tr>
              <td>Date</td>
              <td>{getDate(currentBlock.timestamp)}</td>
            </tr>
            <tr>
              <td>Difficulty</td>
              <td>{currentBlock.difficulty}</td>
            </tr>
            <tr>
              <td>Mined by</td>
              <td>
                <IdentityBadge compact={true} entity={currentBlock.miner} />
              </td>
            </tr>
          </tbody>
        </table>
      </Box>
      <br />
      <TransactionList transactions={transactions} />
    </div>
  )
}

Transactions.propTypes = {
  blockNumber: PropTypes.string
}

Transactions.defaultProps = {
  blockNumber: ''
}

export default Transactions
