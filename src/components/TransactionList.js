import React from 'react'
import PropTypes from 'prop-types'
import {
  Table,
  TableHeader,
  TableRow,
  TableCell,
  LoadingRing,
  TransactionBadge,
  IdentityBadge,
  IconArrowRight,
  Tag,
  textStyle
} from '@aragon/ui'
import { IconTag, LoadingStyle, HeaderStyle } from './styles'
import { useMediaQuery } from 'react-responsive'

const TransactionList = ({ transactions }) => {
  const isTabletOrMobileDevice = useMediaQuery({
    query: '(max-device-width: 1224px)'
  })
  if (!transactions.length) {
    return (
      <LoadingStyle className="loading">
        <LoadingRing />
        <div>Fetching transactions</div>
      </LoadingStyle>
    )
  } else {
    return (
      <>
        <HeaderStyle>
          <h1
            css={`
              ${textStyle('title4')};
            `}
          >
            Transactions ({transactions.length})
          </h1>
        </HeaderStyle>
        <Table
          header={
            <TableRow>
              <TableHeader title="Hash" />
              {!isTabletOrMobileDevice && <TableHeader title="From - To" />}
              <TableHeader title="Value" />
            </TableRow>
          }
        >
          {transactions.map(transaction => (
            <TableRow key={transaction.hash}>
              <TableCell>
                <TransactionBadge
                  transaction={transaction.hash}
                  disabled={true}
                />
              </TableCell>
              {!isTabletOrMobileDevice && (
                <TableCell>
                  <IconTag>
                    <IdentityBadge entity={transaction.from} compact={true} />
                    <IconArrowRight />
                    <IdentityBadge compact={true} entity={transaction.from} />
                  </IconTag>
                </TableCell>
              )}
              <TableCell>
                <Tag mode="new">{transaction.valueConverted} Ether</Tag>
              </TableCell>
            </TableRow>
          ))}
        </Table>
      </>
    )
  }
}

TransactionList.propTypes = {
  transactions: PropTypes.array
}

TransactionList.defaultProps = {
  transactions: []
}

export default TransactionList
