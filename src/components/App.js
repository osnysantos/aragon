import { hot } from 'react-hot-loader/root'
import React, { useEffect, useState } from 'react'
import { Main } from '@aragon/ui'
import { Router, Link } from '@reach/router'
import Blocks from './Blocks'
import Transactions from './Transactions'
import { Provider } from './../store'
import * as actions from './../actions.js'
import { FooterTag, HeaderTag, Layout } from './styles'
import logo from './../../public/aragon-logo.svg'
import { getUniqueBlocks } from '../utils'

const App = () => {
  const [blocks, setBlocks] = useState([])
  const [autoFetch, setAutoFetch] = useState(true)
  const [currentBlock, setCurrentBlock] = useState({})
  const [transactions, setTransactions] = useState({})

  useEffect(() => {
    console.warn('win', window.location)
    if (window.location.pathname === '/') {
      setCurrentBlock(null)
      setTransactions([])
      actions.getLatestBlocks(10).then(value => {
        setBlocks(getUniqueBlocks(value, blocks))
      })
    }
  }, [autoFetch ? blocks : undefined])

  const context = {
    blocks,
    currentBlock,
    transactions,
    autoFetch,
    setBlocks,
    setCurrentBlock,
    setTransactions,
    setAutoFetch
  }

  console.log('context', context)

  return (
    <Provider value={context}>
      <Main theme="dark" layout={true}>
        <Layout>
          <HeaderTag>
            <Link to="/">
              <img src={logo} height={60} alt="Aragon logo" />
            </Link>
          </HeaderTag>
          <Router>
            <Blocks path="/" />
            <Transactions path="/block/:blockNumber" />
          </Router>
          <FooterTag>
            Made with{' '}
            <span role="img" aria-label="Heart icon">
              ❤️
            </span>{' '}
            in{' '}
            <span role="img" aria-label="Brazil flag">
              🇧🇷
            </span>{' '}
            by Osny Netto
          </FooterTag>
        </Layout>
      </Main>
    </Provider>
  )
}

export default hot(App)
