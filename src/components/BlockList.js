import React from 'react'
import PropTypes from 'prop-types'
import {
  Table,
  TableHeader,
  TableRow,
  TableCell,
  IdentityBadge,
  textStyle,
  IconClock,
  Text,
  Button
} from '@aragon/ui'
import { dateFromNow } from '../utils.js'
import { IconTag } from './styles'
import { useMediaQuery } from 'react-responsive'

const BlockList = ({ blocks, goToTransaction }) => {
  const isTabletOrMobileDevice = useMediaQuery({
    query: '(max-device-width: 1224px)'
  })
  return isTabletOrMobileDevice ? (
    <Table
      css={`
        ${textStyle('body4')};
      `}
      header={
        <TableRow>
          <TableHeader title="Block" />
          <TableHeader title="N of Txns" />
        </TableRow>
      }
    >
      {blocks.map(block => (
        <TableRow key={block.number}>
          <TableCell>
            <div>
              <Text>#{block.number}</Text>
              <Text>
                <IconTag>
                  <IconClock />
                  {dateFromNow(block.timestamp)}
                </IconTag>
              </Text>
              <Text>
                <IdentityBadge compact={true} entity={block.miner} />
              </Text>
            </div>
          </TableCell>
          <TableCell>
            {block.transactions.length} txns
            <Button
              mode="strong"
              size="small"
              onClick={() => goToTransaction(block)}
            >
              View
            </Button>
          </TableCell>
        </TableRow>
      ))}
    </Table>
  ) : (
    <Table
      header={
        <TableRow>
          <TableHeader title="Block" />
          <TableHeader title="Date" />
          <TableHeader title="Miner" />
          <TableHeader title="N of transactions" />
        </TableRow>
      }
    >
      {blocks.map(block => (
        <TableRow key={block.number}>
          <TableCell>#{block.number}</TableCell>
          <TableCell
            css={`
              ${textStyle('body3')}
            `}
          >
            <IconTag>
              <IconClock />
              {dateFromNow(block.timestamp)}
            </IconTag>
          </TableCell>
          <TableCell>
            <Text>
              Mined by <IdentityBadge compact={true} entity={block.miner} />
            </Text>
          </TableCell>
          <TableCell>
            {block.transactions.length} transactions
            <Button
              mode="strong"
              size="small"
              onClick={() => goToTransaction(block)}
            >
              View
            </Button>
          </TableCell>
        </TableRow>
      ))}
    </Table>
  )
}

BlockList.propTypes = {
  blocks: PropTypes.array,
  goToTransaction: PropTypes.func
}

BlockList.defaultProps = {
  blocks: [],
  goToTransaction: () => {}
}

export default BlockList
