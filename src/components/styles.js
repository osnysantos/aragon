import styled from 'styled-components'

export const HeaderTag = styled.div`
  padding: 10px;
  display: flex;
  justify-content: center;
  margin-bottom: 20px;
  img {
    display: flex;
    transition: transform 0.2s ease-in;
    &:hover {
      transform: scale(1.1);
    }
  }
`

export const FooterTag = styled.div`
  text-align: center;
  font-size: 13px;
  margin: 30px 0;
  color: rgba(255, 255, 255, 0.5);
`

export const IconTag = styled.div`
  display: flex;
  align-items: center;
`

export const HeaderStyle = styled.div`
  margin-bottom: 1rem;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
`

export const LoadingStyle = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 4rem;
  > span > span {
    transform: scale(2);
  }
  div {
    display: block;
    margin: 1rem;
  }
`
export const Layout = styled.div`
  margin: 0 1rem;
`
