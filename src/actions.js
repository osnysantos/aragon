const API_KEY = 'https://mainnet.infura.io/v3/8941a08f4bb24f53845aba586282fbed'
const Web3 = require('web3')
const w3 = new Web3(new Web3.providers.HttpProvider(API_KEY))
const BLOCKS_TO_LOAD = 10
async function getBlock(hash) {
  const block = await w3.eth.getBlock(hash)
  return block
}

async function getLatestBlocks(limit = BLOCKS_TO_LOAD) {
  const latestBlockNumber = await w3.eth.getBlockNumber()

  let blocks = []
  for (let i = latestBlockNumber; i > latestBlockNumber - limit; i--) {
    let block = await w3.eth.getBlock(i - 1)
    blocks.push(block)
  }
  return blocks
}

async function getTransactions(transactionsArr) {
  let transactions = []
  for (let i = 0; i < transactionsArr.length; i++) {
    const item = await w3.eth.getTransaction(transactionsArr[i])
    if (item.value && item.value !== '0' && item.value !== 'undefined') {
      let hexNumber = w3.utils.numberToHex(String(item.value))
      hexNumber = w3.utils.fromWei(hexNumber)
      item.valueConverted = hexNumber
    }
    transactions.push(item)
  }
  return transactions
}

export { getLatestBlocks, getTransactions, getBlock }
