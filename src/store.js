import React from 'react'

const storeContext = React.createContext({
  blocks: [],
  currentBlock: null,
  transactions: null,
  autoFetch: true,
  setBlocks: () => {},
  setCurrentBlock: () => {},
  setAutoFetch: () => {}
})

export const Provider = storeContext.Provider
export const Consumer = storeContext.Consumer

export default storeContext
