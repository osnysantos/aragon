import * as moment from 'moment'

function dateFromNow(timestamp) {
  return moment.unix(timestamp).fromNow()
}

function getDate(timestamp) {
  return moment.unix(timestamp).format('MMMM Do YYYY, h:mm:ss a')
}

function filterEtherTransactions(transactions) {
  return transactions.filter(transaction => transaction.value !== '0')
}

function getUniqueBlocks(newBlocks, oldBlocks) {
  let blocks = [...oldBlocks]
  if (newBlocks.length) {
    let blockNumbers = oldBlocks.map(block => block.number)
    let newValues = newBlocks.filter(
      block => !blockNumbers.includes(block.number)
    )
    if (newValues.length) {
      blocks = [...newValues, ...blocks]
    }
  }
  return blocks
}

export { getDate, dateFromNow, filterEtherTransactions, getUniqueBlocks }
