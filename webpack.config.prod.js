const merge = require('webpack-merge')
const baseConfig = require('./webpack.config.base')
const CopyPlugin = require('copy-webpack-plugin')

module.exports = merge(baseConfig, {
  mode: 'production',
  plugins: [new CopyPlugin([{ from: './aragon-ui', to: './aragon-ui' }])]
})
