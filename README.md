# Aragon

This projects display the latest blocks from Ethereum and it's list of transactions.

## Live demo

[Check it out](https://aragon-osny.surge.sh)

## Dev

1. `npm install`
2. `npm start`

## Running the tests

`npm run test` or `npm run test:watch`

## Build

`npm run build`

## Deployment

The project is hosted by [Surge](https://surge.sh)

To deploy it: `npm run build; surge ./dist`

# Author

Made with :heart: by Osny Netto
